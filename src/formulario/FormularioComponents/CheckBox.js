import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Checkbox from '@material-ui/core/Checkbox';

class CheckBox extends React.Component
{
    render ()
    {
       return( <div >
        <FormControl component="fieldset" >
          <FormLabel component="legend">Assign responsibility</FormLabel>
          <FormGroup>
            <FormControlLabel
              control={<Checkbox  name="gilad" />}
              label="Gilad Gray"
            />
            <FormControlLabel
              control={<Checkbox  name="jason" />}
              label="Jason Killian"
            />
            <FormControlLabel
              control={<Checkbox  name="antoine" />}
              label="Antoine Llorca"
            />
          </FormGroup>
          <FormHelperText>Be careful</FormHelperText>
        </FormControl>
        <FormControl component="fieldset" >
          <FormLabel component="legend">Pick two</FormLabel>
          <FormGroup>
            <FormControlLabel
              control={<Checkbox name="gilad" />}
              label="Gilad Gray"
            />
            <FormControlLabel
              control={<Checkbox  name="jason" />}
              label="Jason Killian"
            />
            <FormControlLabel
              control={<Checkbox  name="antoine" />}
              label="Antoine Llorca"
            />
          </FormGroup>
          <FormHelperText>You can display an error</FormHelperText>
        </FormControl>
      </div>)
    }
}
export default CheckBox;