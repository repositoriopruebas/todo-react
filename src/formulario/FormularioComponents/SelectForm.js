import React from 'react';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import currencies from '../Services/data';
import styles from '../Styles/StylesForm'

class SelectForm extends React.Component
{
   
    constructor(props)
    {
        super(props)
        this.state = {
            options:[],
            charge:false   
        }
    } 
    componentDidMount()
    {
       this.setState({charge: true, options: currencies})  
    }
       
    render()
    {
        const{charge,options} = this.state;
        if(charge == false)
        {
            return "cargando"
        }

       return   <div >

                    <TextField style={{ width: 410 }} label="Señala opción"                                       
                        select >                                 
                        {options.map((op) => (
                            <option key={op.value} value={op.value}>
                            {op.label}
                            </option>
                    ))}
                    </TextField><br/>
        
                </div> 
             
        
    }
  
}
export default SelectForm   