import React from 'react';
import ReactDOM from 'react-dom';
import ButtonForm from './FormularioComponents/ButtonForm';
import InputFormText from './FormularioComponents/InputFormText';
import SelectForm from './FormularioComponents/SelectForm';
import CheckBox from './FormularioComponents/CheckBox';
import Styles from './Styles/StylesForm';


class FormularioMi extends React.Component
{
    render()
    {
        return (
            <form className={Styles.root}  noValidate autoComplete="off">
                <InputFormText placeholder="Introduce nombre de usuario"/>
                <SelectForm />
                <CheckBox />
                <ButtonForm></ButtonForm> 
            </form>                  
        ) 
    }
}

export default FormularioMi;