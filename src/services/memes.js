export async function getMemes()
{
    const response = await fetch('https://api.imgflip.com/get_memes')
    const responseJson = await response.json()

    return responseJson

}