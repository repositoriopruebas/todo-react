import React from 'react';
import PropTypes from 'prop-types'
const Meme = ({nombre,imageurl,ancho,largo}) => 
                            <div className="single-meme">
                                <h6>{nombre}</h6>  
                                <img src={imageurl} alt={nombre}/> 
                                <p>Ancho {ancho} X </p>                       
                                <p>Largo {largo}</p>
                            </div> 
Meme.propTypes = 
{
    nombre:PropTypes.string.isRequired
}
export default Meme;
