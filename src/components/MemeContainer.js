import React from 'react'
import Meme from './Meme'
import {getMemes} from '../services/memes'
class MemeContainer extends React.Component {
    constructor(props)
    {
        super(props);
        this.state = {
            memes:[],
            isFetch:true,
        }
    }
    async componentDidMount()
    {       
        const responseJson = await getMemes()
        this.setState({memes:responseJson.data.memes,isFetch:false})
        console.log(responseJson)
    }
    componentDidUpdate()
    {
        console.log('updated');
    }
    render ()
    {   
        const {isFetch, memes} = this.state
        if(isFetch)
        {
            return "Loading";
        }
        const name = this.state.memes        
        console.log(name)
        return (
            <section className="memes-container">
             {
                 this.state.memes.map((meme) => <Meme ancho={meme.width} largo={meme.height} imageurl={meme.url} nombre={meme.name} key={meme.id}/>)   
             }       
            
            </section>
        )
    }
}
export default MemeContainer