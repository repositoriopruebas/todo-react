import logo from './logo.svg';
import './App.css';
import Hola from './hola';
import Contador from './Contador';
import MemeContainer from './components/MemeContainer';
import FormularioMi from './formulario/FormularioMi';
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <div className="App-intro">
            {/*Incluimos nuestro componente*/}

            <Hola nombre="Pepito"/>
            <Contador/>
            <MemeContainer/>
            <FormularioMi />
        </div>
      </header>
   
    </div>
  );
}

export default App;
